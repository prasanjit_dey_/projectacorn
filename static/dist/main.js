(function () {
    'use strict';

    var app=angular
        .module('acornApp', [
            'acornApp.config',
            //'ngAnimate',
            'acornApp.routes',
            'acornApp.home',
            'acornApp.hotelSearch',
            'acornApp.hotels',
            'acornApp.hotel',
            'acornApp.HttpService',
            'acornApp.aboutUs',
            'acornApp.preRegistration',
            'ngSanitize',
            'ui.select',
            'ui.bootstrap',
            'ngNotificationsBar'
        ]);

    angular.module('acornApp.config', []);

    app.config(['notificationsConfigProvider', function (notificationsConfigProvider) {
        // auto hide
        notificationsConfigProvider.setAutoHide(true);

        // delay before hide
        notificationsConfigProvider.setHideDelay(3000);
        // Set an animation for hiding the notification
        //notificationsConfigProvider.setAutoHideAnimation('fadeOutNotifications');
    }]);


    /*angular.module('acornApp.HttpService', []);*/




    // Module specific configuration
    /*****************base path use for api hitting bars url ***************     /*angular.module('acornApp.config')
     .value('acornApp.config', {

     // basePath: 'http://52.74.163.60/' // Set your base path here
     });*/




     angular.module('acornApp.routes', ['ngRoute', 'ui.router', 'base64']).run(['$rootScope', function($rootScope) {
        /*$rootScope.static_path = 'https://healthbrio.s3.amazonaws.com/';*/
         $rootScope.static_path = 'http://acornhotels.com/static/';
         $rootScope.basePath = 'http://acornhotels.com/';
        /*$rootScope.static_path = 'http://192.168.56.1:8000/static/';*/
        /*$rootScope.basePath = 'http://127.0.0.1:8000/';*/

        $rootScope.$on('$stateChangeSuccess',
            function(event, toState, toParams, fromState, fromParams){
                // alert(toState.name);
                document.body.scrollTop = document.documentElement.scrollTop = 0;
                if(toState.name==='/'){
                    $('html,Body').css({
                        'overflow' : 'hidden',
                        'height' : '100%'
                    });
                }else{
                    $('Body').css({
                        'overflow' : 'scroll',
                        'height' : '100%'
                    });
                }

            });
        }]);




})();

(function () {
  'use strict';

  angular.module('acornApp.config')
    .config(config);

    config.$inject = ['$locationProvider'];

   /** @name config
   * @desc Enable HTML5 routing*/

  function config($locationProvider) {
    $locationProvider.html5Mode({
      enabled: false,
      requireBase: false
    });
   
    $locationProvider.hashPrefix('');
  }


})();

(function () {
  'use strict';

  angular
    .module('acornApp.routes')
    .config(indexConfig);

  indexConfig.$inject = ['$stateProvider', '$urlRouterProvider'];

/**
   * @name config
   * @desc Define valid application routes*/

   //***  stateProvider for Symptoms*****
   window.static_path = 'http://acornhotels.com/static/';
  /*window.static_path = 'http://127.0.0.1:8000/static/';*/


  /*window.static_path = 'https://healthbrio.s3.amazonaws.com/';*/

  function indexConfig($stateProvider, $urlRouterProvider){
    $urlRouterProvider.otherwise('/');
    $stateProvider
        /*********home page routing ***********/
        /*.state('home', {
            url: '/home',
            controller:'HomeController',
            controllerAs:'home_vm',
            templateUrl: static_path+'templates/home/home.html'
        })*/
        /**********hotelSearchrouting NEw Pardeep ****************/
        .state('home',{
            url:'/',
            controller:'HomeController',
            controllerAs:'home_vm',
            templateUrl:static_path+'templates/home/home.html'
        })

        /*****************hotels search result list****************/
        .state('hotels',{
            url:'/hotels/:hotel_city_id/:hotel_city/:checkin/:checkout',
            controller:'HotelsController',
            controllerAs:'hotels_vm',
            templateUrl:static_path+'templates/hotels/hotels.html'
        })
        /*****************hotel Detail ***********************/
        .state('hotel',{
            url:'/hotel',
            controller:'HotelController',
            controllerAs:'hotel_vm',
            templateUrl:static_path+'templates/hotel/hotel.html'
        })
        /**********about US Routinf ****************/
        .state('aboutUs',{
            url:'/aboutUs',
            controller : 'AboutUsController',
            controllerAs:'avoutUs_vm',
            /*controller : function($scope, $timeout){
                alert("sdfd");
                $scope.contactUsTabClick = function()
                {
                    alert("sdafds");
                    $timeout(function(){
                        $(".modal-backdrop").css("z-index", "0");
                    });
                };
            },*/
            //controller:'AboutUsController',
            //controllerAs:'aboutUs_vm',
            templateUrl:static_path+'templates/aboutUs/aboutUs.html'
        })
        .state('terms',{
            url:'/terms',
            controller : function($scope, $timeout){
                $scope.contactUsTabClick = function(){
                    $timeout(function(){
                        $(".modal-backdrop").css("z-index", "0");
                    });
                };
            },
            templateUrl:static_path+'templates/terms/terms.html'
        })
        .state('policy',{
            url:'/policy',
            controller : function($scope, $timeout){
                $scope.contactUsTabClick = function(){
                    $timeout(function(){
                        $(".modal-backdrop").css("z-index", "0");
                    });
                };
            },
            //template:'<h1>Policy</h1>'
            templateUrl:static_path+'templates/policy/policy.html'
        })
        .state('cancelation',{
            url:'/cancellationPolicy',
            controller : function($scope, $timeout){
                $scope.contactUsTabClick = function(){
                    $timeout(function(){
                        $(".modal-backdrop").css("z-index", "0");
                    });
                };
            },
            //template:'<h1>Policy</h1>'
            templateUrl:static_path+'templates/cancelation/cancelation.html'
        })
        .state('preregistration',{
            url:'/pre-registration/:hotel_name',
            controller:'PreRegistrationController',
            controllerAs:'preRegistration_vm',
            templateUrl:static_path +'templates/preregistration/preregistration.html'
        });

  }
})();
(function (){
    'use strict';

    angular.module('acornApp.HttpService',[])
        .service('HttpService', HttpServiceFun);
        
    HttpServiceFun.$inject=['$http','$rootScope'];
       
    function HttpServiceFun($http, $rootScope){
        return{
              PrivateServiceGet: PrivateServiceGetFun,
              PublicServiceGet: PublicServiceGetFun,
              publicServiceGetwithString:publicServiceGetwithStringFun,
              PrivateServicePost:PrivateServicePostFun,
              PublicServicePost:PublicServicePostFun
        };

        function PrivateServiceGetFun(url_){
             return $http({
                url : $rootScope.basePath + url_,
                method: 'GET',
                headers : {
                     'Content-Type' : 'application/json'
                }
            });

        }
        function PublicServiceGetFun(url_){
            return $http({
                url :  $rootScope.basePath +url_,
                method: 'GET',
                headers : {
                     'Content-Type' : 'application/json'

                }
            });
        }
        function publicServiceGetwithStringFun(url_ , data_){
            return $http({
                url :  $rootScope.basePath +url_,
                method: 'GET',
                data: data_,
                headers : {
                    'Content-Type' : 'application/json'

                }
            });
        }
        function PrivateServicePostFun(url_, data_){
             return $http({
                url : $rootScope.basePath + url_,
                method: 'POST',
                data: data_,
                headers : {
                     'Content-Type' : 'application/json'
                }
            });
        }
        function PublicServicePostFun(url_, data_){
              return $http({
                url : $rootScope.basePath + url_,
                method: 'POST',
                data: data_,
                headers : {
                     'Content-Type' : 'application/json'
                }
            });
        }

    }


})();

/**
 * Created by Innoticalajay on 10/19/2015.
 */
(function(){
    "use strict";

    angular.module('acornApp.aboutUs',[
        'acornApp.aboutUs.controllers',
        'acornApp.aboutUs.directives',
        'acornApp.aboutUs.services'
    ]);

    angular.module('acornApp.aboutUs.controllers',[]);
    angular.module('acornApp.aboutUs.directives',[]);
    angular.module('acornApp.aboutUs.services',[]);
})();
/**
 * Created by IDCS12 on 9/24/2015.
 */
(function(){
    'use strict';

    angular.module('acornApp.home',[
        'acornApp.home.controllers',
        'acornApp.home.directives',
        'acornApp.home.services'
    ]);


    console.log("home getter");
    angular.module('acornApp.home.controllers',[]);

    angular.module('acornApp.home.directives',[]);

    angular.module('acornApp.home.services',[]);
})();
/**
 * Created by IDCS12 on 9/24/2015.
 */
(function(){
    'use strict';

    angular.module('acornApp.hotel',[
        'acornApp.hotel.controllers',
        'acornApp.hotel.directives',
        'acornApp.hotel.services'
    ]);


    console.log("hotel detail getter");
    angular.module('acornApp.hotel.controllers',[]);

    angular.module('acornApp.hotel.directives',[]);

    angular.module('acornApp.hotel.services',[]);
})();
/**
 * Created by IDCS12 on 9/24/2015.
 */
(function(){
    'use strict';

    angular.module('acornApp.hotels',[
        'acornApp.hotels.controllers',
        'acornApp.hotels.directives',
        'acornApp.hotels.services'
    ]);


    console.log("hotels list getter");
    angular.module('acornApp.hotels.controllers',[]);

    angular.module('acornApp.hotels.directives',[]);

    angular.module('acornApp.hotels.services',[]);
})();
/**
 * Created by IDCS12 on 9/24/2015.
 */
(function(){
    'use strict';

    angular.module('acornApp.hotelSearch',[
        'acornApp.hotelSearch.controllers',
        'acornApp.hotelSearch.directives',
        'acornApp.hotelSearch.services'
    ]);


    console.log("hotelSearch getter");
    angular.module('acornApp.hotelSearch.controllers',[]);

    angular.module('acornApp.hotelSearch.directives',[]);

    angular.module('acornApp.hotelSearch.services',[]);
})();
/**
 * Created by Innoticalajay on 11/20/2015.
 */
(function(){
    'use strict';

    angular.module('acornApp.preRegistration',[
        'acornApp.preRegistration.controllers',
        'acornApp.preRegistration.directives',
        'acornApp.preRegistration.services',
    ]);

    angular.module('acornApp.preRegistration.controllers',[]);
    angular.module('acornApp.preRegistration.directives',[]);
    angular.module('acornApp.preRegistration.services',[]);

})();
/**
 * Created by Innoticalajay on 10/19/2015.
 */
(function(){
    "use strict";

    angular.module('acornApp.aboutUs.controllers')
        .controller('AboutUsController', AboutUsControllerFun);

    AboutUsControllerFun.$inject=['$scope', '$timeout'];

    function AboutUsControllerFun($scope, $timeout){
        var vm = this;
        vm.contactUsTabClick = function(){
            $timeout(function(){
                $(".modal-backdrop").css("z-index", "1");
            });
        };
    }
})();
/**
 * Created by Innoticalajay on 10/19/2015.
 */
(function(){
    "use strict";

    angular.module('acornApp.aboutUs.directives');

})();
/**
 * Created by Innoticalajay on 10/19/2015.
 */
(function(){
    "use strict";

    angular.module('acornApp.aboutUs.services');
        
})();
/**
 * Created by IDCS12 on 3/18/2015.
 */
(function(){
    "use strict";
    angular.module('acornApp.home.controllers')
        .controller('HomeController', HomeControllerFun);

    HomeControllerFun.$inject = ['$http','$scope', '$interval', 'homeService', 'notifications'];
    

    function HomeControllerFun($http, $scope, $interval, homeService, notifications ){
        // var vm=this;
         $http.get($scope.static_path+"javascripts/home/data.json").success(function(data){
          $scope.data= data;
         }); 
        
         $scope.homeList = [];
         
        //  homeService.getdata().success(function(response){
        //     //alert(JSON.stringify(response));
        //     $scope.homeList = response;
        //     //alert($scope.homeList[2].slider_heading);
        //     //alert($scope.homeList[1].slider_image[0].image_url);
        // }
        // ).error(function(error){
        //     alert("error");
        // });

        var vm =this;
		// alert("HotelSearch calls");
    	/*$http.get($scope.static_path+"http://127.0.0.1:8000/api/searchpage/").success(function(data){
          $scope.data= data;
         });*/

        vm.cities=[];
        vm.citySeleted='';
        homeService.cities().success(function(response){
            //alert("success");
            vm.cities = response;
            //console.log("fsdfdsf" + JSON.stringify(response));
        }).error(function(error){
            notifications.showError({message: 'Oh snap ! connection Lost!!!'});
        });

        $scope.closee=function(){
            $('.modal-backdrop').css("position", "static");
        };

    }

})();


/**
 * Created by IDCS12 on 3/19/2015.
 */
(function(){
	'use strict';
	
    angular.module('acornApp.home.directives')
	.directive('logoSlider', logoSliderFun);

	angular.module('acornApp.home.directives')
	.directive('onePageScroll', onePageScrollFun);

    angular.module('acornApp.home.directives')
	.directive('bootselectpicker', bootselectpickerFun);

    angular.module('acornApp.home.directives')
        .directive("dateTimeIn",dateTimeInFun);

    angular.module('acornApp.home.directives')
        .directive("dateTimeInMob",dateTimeInMobFun);

	logoSliderFun.$inject=[];
	onePageScrollFun.$inject=[];
    bootselectpickerFun.$inject=['$timeout'];
    dateTimeInFun.$inject = ['$timeout'];
    dateTimeInMobFun.$inject = ['$timeout'];

	function logoSliderFun(){
		return{
			restrict : 'AE',			
			link : function(scope, element, attrs){
				//alert("Slides");
				// $(element).bxSlider({
				// 	speed: 6000
				// });
				
				// $('#slider').bxSlider({
				// 	maxSlides: 4,
				// 	slideWidth: 170,
				// 	slideMargin: 10,
				// 	ticker: true,
				// 	speed: 6000
				// 	});	
			}
		};
	}
		
	function onePageScrollFun(){
		return{
			restrict:'AE',
			link : function(scope,elements,attrs){
	
				$(".main").onepage_scroll({
			        sectionContainer: "section",
			        animationTime: 2000,
			        responsiveFallback: false,
			        loop: true,
			        pagination: true,
			        updateURL: true, 
			        direction: "vertical",
			        beforeMove: function(index) 
			        {
			            //$(".main").moveTo(2);
			            // alert(index.length.value)
			        }
			      });
			}
		};
	}

    function bootselectpickerFun($timeout){
        return{
            restrict :'AE',
            link : function(scope, element, attrs){
                //alert("sdfds");
                $timeout(function() {
                  /*element.selectpicker();*/

                    $('.selectpicker').selectpicker();
               });

            }
        };
    }





    function dateTimeInFun($timeout){
        //alert("Date Time Directive");
        return{
            restrict: 'AE',

            link: function(scope,ele,attr){
                $("#checkin").datepicker({
                    dateFormat : "yy-mm-dd",
                    minDate:0,
                    changeMonth: true,
                    changeYear: true,
                    onSelect: function(date) {
                        //  alert(date);
                        $("#checkout").datepicker("destroy");
                        var checkindate = document.getElementById("checkin").value;

                        scope.$apply(function(){
                            scope.checkin_date= checkindate;
                        });
                        console.log(checkindate);

                        $('#checkout').datepicker({
                            minDate :  checkindate,
                            dateFormat : 'yy-mm-dd',
                            changeMonth: true,
                            changeYear: true,
                            });
                    }
                });
            }
        };
    }
    function dateTimeInMobFun($timeout){
        //alert("Date Time Directive");
        return{
            restrict: 'AE',
            link: function(scope,ele,attr){
                $("#checkinmob").datepicker({
                    dateFormat : "yy-mm-dd",
                    minDate:0,
                    changeMonth: true,
                    changeYear: true,
                    onSelect: function(date) {
                        //  alert(date);
                        $("#checkoutmob").datepicker("destroy");
                        var checkindate = document.getElementById("checkinmob").value;
                        scope.$apply(function(){
                            scope.checkin_date= checkindate;
                        });
                        console.log(checkindate);

                        $('#checkoutmob').datepicker({
                            minDate :  checkindate,
                            dateFormat : 'yy-mm-dd',
                            changeMonth: true,
                            changeYear: true,
                            });
                    }
                });
            }
        };
    }
	
	
})();
/**
 * Created by IDCS12 on 3/19/2015.
 */
(function(){
    'use strict';
    angular.module('acornApp.home.services')
        .service('homeService', homeServiceFun);
        
        homeServiceFun.$inject = ['HttpService'];

        function homeServiceFun(HttpService){
            return{
                cities:cities
            };
            function cities(){
                return (HttpService.PublicServiceGet('api/city/'));
            }
             /*this.getdata = function(){
                    
                    return HttpService.PublicServiceGet('api/homeslider/');
                }*/
        }

})();
/**
 * Created by IDCS12 on 9/24/2015.
 */
(function(){
    "use strict";
    angular.module('acornApp.hotel.controllers')
        .controller('HotelController', HotelControllerFun);

    HotelControllerFun.$inject = ['$scope', '$interval'];
    

    function HotelControllerFun($scope, $interval){
        var vm=this;
		//alert("Hotel list calls");
    }

})();


/**
 * Created by IDCS12 on 3/24/2015.
 */
(function(){
	'use strict';

    angular.module('acornApp.hotel.directives',[])
        .directive('galleryDir',galleryDirFun);

    galleryDirFun.$inejct=[];

        function galleryDirFun(){
            return {
                restrict: 'EA',
                link : function(scope, element, attrs){
                    //alert("sdafsd");
                    //$("a.fancybox").fancybox();
                    element.bind('click', function(event){
                        scope.$apply(function(){
                            $(".fancybox").removeClass("fancybox");
                            element.children().children().addClass("fancybox");
                            //alert(element.children().length);
                            $("a.fancybox").fancybox();
                        });
                    });

                    /*$(document).ready(function(){
                           $("a.fancybox").fancybox();
                    });*/
                }
            };
        }
})();
/**
 * Created by IDCS12 on 9/24/2015.
 */
(function(){
    'use strict';
    angular.module('acornApp.hotel.services')
        .service('hotelService', hotelService);

    function hotelService(){
    	
    }
    
})();
/**
 * Created by IDCS12 on 9/24/2015.
 */
(function(){
    "use strict";
    angular.module('acornApp.hotels.controllers')
        .controller('HotelsController', HotelsControllerFun);

    HotelsControllerFun.$inject = ['$scope', '$interval', '$stateParams', 'hotelsService', '$http', '$timeout', '$filter' , 'notifications'];
    

    function HotelsControllerFun($scope, $interval, $stateParams, hotelsService, $http, $timeout,$filter, notifications){
        var vm=this;
		vm.hotelsList = [];
        vm.cities=[];
        vm.previouscity = $stateParams.hotel_city;
        vm.previouscityId=$stateParams.hotel_city_id;
        $scope.checkin_date = decodeURIComponent($stateParams.checkin);
        vm.checkout_date = decodeURIComponent($stateParams.checkout);
        //alert("after decode" + $scope.checkin_date +" "+ vm.checkout_date );
        //var newdate = vm.checkin_date.split("/").reverse().join("-");
        //vm.checkinDate_ = $filter('date')(new Date(Date.parse($scope.checkin_date)), 'yyyy-MM-dd');
        //vm.checkoutDate_ = $filter('date')(new Date(Date.parse(vm.checkout_date)), 'yyyy-MM-dd');
        //alert("after parsing " + vm.checkinDate_ + " " + vm.checkoutDate_);
        //alert(new Date(Date.parse(vm.checkin_date)).getMonth()+"again decode" +$filter('date')(vm.checkin_date, "yyyy-MM-dd"));
        vm.nights ='';
        /**************night caluclation *********************/
         if ($scope.checkin_date && vm.checkout_date) {
            var difference_msec = Date.parse(vm.checkout_date) - Date.parse($scope.checkin_date) ;
            vm.nights=  difference_msec / 86400000;
        }
        //alert($stateParams.hotel_city + vm.checkin_date + vm.checkout_date);

        vm.citySeleted={
            id:$stateParams.hotel_city_id,
            city_name:$stateParams.hotel_city
        };

        vm.sortby_data=[];
        vm.selectedSortData='';

        /*getting city data */
        hotelsService.searchPageMisc().success(function(response){
           //alert("success");
            vm.cities = response.city_data;
            console.log("cities"+ response.city_data);
            for(var i =0; i<response.city_data.length;i++){
                if(response.city_data[i].id == vm.previouscityId){
                    //alert("city id found" +response.city_data[i].id);
                    //vm.citySeleted.id = response.city_data[i].id;
                    //vm.citySeleted.city_name = response.city_data[i].city_name;
                    break;
                }else{
                   //console.log("City Name not found from the api ")
                }
            }
            vm.sortby_data=response.sortby_data;
            vm.selectedSortData=response.sortby_data[0];
            console.log(JSON.stringify(response.sortby_data));

        }).error(function(){
            notifications.showError({message: 'Oh snap ! city selection connection Lost!!!'});
        });
        //alert("id is" +vm.previouscityId);

        vm.cityFilterData = {
            city : vm.previouscityId,
            min_price :0,
            max_price :5000,
            check_in:$scope.checkin_date,
            check_out:vm.checkout_date,
            sortby:'rating'
        };



        /*getting all hotel list data*/
        /*//city=2&min_price=0&max_price=1800&check_in=2015-01-01&check_out=2015-01-01&sortby=rating*/
        hotelsService.allHotels(vm.cityFilterData).success(function(response){
            //alert("success");
            vm.hotelsList = response;
            //alert(JSON.stringify(response));
        }).error(function(error){
            notifications.showError({message: 'Oh snap ! hotels list connection Lost!!!'});
        });
        vm.changeSearchData = function(city,checkin, checkout){
             //console.log("this function ality need to be done" + city + checkin + checkout);

            /*changing that city selected is */
            if(typeof(city)=='object'){
                vm.cityFilterData = {
                    city : city.id,
                    min_price :0,
                    max_price :5000,
                    check_in:checkin,
                    check_out:checkout,
                    sortby:'rating'
                };
                hotelsService.allHotels(vm.cityFilterData).success(function(response){
                    vm.hotelsList = response;
                }).error(function(error){
                    notifications.showError({message: 'Oh snap ! hotels list connection Lost!!!'});
                });
            }else{
                vm.cityFilterData = {
                    city : vm.previouscityId,
                    min_price :0,
                    max_price :5000,
                    check_in:checkin,
                    check_out:checkout,
                    sortby:'rating'
                };
                hotelsService.allHotels(vm.cityFilterData).success(function(response){
                    vm.hotelsList = response;
                }).error(function(error){
                    notifications.showError({message: 'Oh snap ! hotels list connection Lost!!!'});
                });
            }
            if (checkin && checkout) {
                var difference_msec = Date.parse(checkout) - Date.parse(checkin) ;
                vm.nights=  difference_msec / 86400000;
            }
           // alert(JSON.stringify(vm.citySeleted));
            /*alert(JSON.stringify(city.city_name) +" " +vm.checkin_date+" "+checkout );
            //console.log(vm.selectedCity.city_name + ' '+ vm.selectedCity.id + ' '+vm.checkin_date +' '+ vm.checkout_date);
            vm.cityFilterData = {
                city :2,
                min_price :0,
                max_price :2000,
                check_in:2015-01-01,
                check_out:2015-01-01,
                sortby:'rating'
            };
            hotelsService.allHotels(vm.cityFilterData).success(function(response){
                //alert("success");
                vm.hotelsList = response;
                console.log(response);
            }).error(function(error){
                alert("error");
            });*/
        };

        vm.contactUsTabClick = function(){
            $timeout(function(){
                $(".modal-backdrop").css("z-index", "0");
            });
        };
    }

})();

/**
 * Created by IDCS12 on 3/24/2015.
 */
(function(){
	'use strict';

    angular.module('acornApp.hotels.directives',[])
        .directive('myMaps', myMapsFun);



    myMapsFun.$inejct=[];


    function myMapsFun(){
        return{
            restrict:'E',
            template :'<div></div>',
            replace:'true',
            link:function(scope,element,attrs){
                //alert(scope.$eval(attrs.latitude));
                var latitude =scope.$eval(attrs.latitude);
                var longitude =scope.$eval(attrs.longitude);
                var myLatLng =  new google.maps.LatLng(latitude,longitude);
                var mapOptions = {
                  center:myLatLng,
                  zoom: 8,
                  mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                var map = new google.maps.Map(document.getElementById(attrs.id),mapOptions);
                var marker = new google.maps.Marker({
                      position: myLatLng,
                      map: map,
                      title: 'My Home!'
                  });

                marker.setMap(map);
                marker.setAnimation(google.maps.Animation.BOUNCE);
            }
        };
    }




	
})();
/**
 * Created by IDCS12 on 9/24/2015.
 */
(function(){
    'use strict';
    angular.module('acornApp.hotels.services')
        .factory('hotelsService', hotelsService);
    hotelsService.$inject = ['HttpService'];
    function hotelsService(HttpService){
    	return{
            searchPageMisc:searchPageMiscFun,
            allHotels : allHotels,
            filterHotel : filterHotel
        };

        function searchPageMiscFun(){
            return (HttpService.PublicServiceGet('api/searchpagemisc/'));
        }

        function allHotels(data){
            //city=2&min_price=0&max_price=1800&check_in=2015-01-01&check_out=2015-01-01&sortby=rating
            return (HttpService.publicServiceGetwithString('api/hotel/?city=' +data.city + '&min_price=' + data.min_price + '&max_price=' + data.max_price +'&check_in='+data.check_in+'&check_out='+data.check_out+'&sortby='+data.sortby));
        }

        function filterHotel(){
            return true;
        }

    }
})();
/**
 * Created by IDCS12 on 3/18/2015.
 */
(function(){
    "use strict";
    angular.module('acornApp.hotelSearch.controllers')
        .controller('HotelSearchController', HotelSearchControllerFun);


    HotelSearchControllerFun.$inject = ['$scope','$http', '$interval', 'hotelSearchService'];
    

    function HotelSearchControllerFun($scope, $http, $interval, hotelSearchService){
        var vm=this;
		// alert("HotelSearch calls");
    	/*$http.get($scope.static_path+"http://127.0.0.1:8000/api/searchpage/").success(function(data){
          $scope.data= data;
         });*/
         
        vm.cities=[];

        hotelSearchService.cities().success(function(response){
            //alert("success");
            vm.cities = response;
            vm.citySeleted='';
            console.log(JSON.stringify(response));
        }).error(function(){
            // alert("error");
        });
        

        
        $scope.closee=function(){
                $('.modal-backdrop').css("position", "static");
        };

         
    }

})();


/**
 * Created by IDCS12 on 3/19/2015.
 */
(function(){
	'use strict';
    angular.module('acornApp.hotelSearch.directives')
	.directive("select",selectFun);
	
	// angular.module('acornApp.hotelSearch.directives')
	// .directive("datedirective",datedirectiveFun);
	
	selectFun.$inject= [];
	// datedirectiveFun.$inject=[];
	
	function selectFun(){
		// alert("hello");
		return{
			restrict: 'AE',
			link: function(scope,ele,attr){
				$( ".dropdown" ).selectmenu();
				scope.$on("$stateChangeSuccess", function(){
					$( ".ui-selectmenu-button" ).css("height", "80px");
					// $(".ui-selectmenu-text").css("color","white","background","white");
				});
				
			}
		};
	}
	
// 	function datedirectiveFun(){
// 		return{
// 			restrict: 'AE',
// 			link: function(scope,ele,attr){
// 				// alert("hello directive");
// 				$('#checkin').datepicker({
// 					minDate:0,
					
// 					var checkin:document.getElementById("checkin").value,
// 				});
							
// 				var abc= $("#checkin").attr("ng-modal").val();
// 				alert(abc);
				
				
// 				// scope.checkOutBlur= function(){
// 				// 	alert("ssdf");
// 				// 	$("#checkout").datepicker("destroy");	
// 				// 	var checkin=document.getElementById("checkin").value;
// 			    //       console.log(checkin);
// 			    //       $('#checkout').datepicker({
// 			    //         minDate :  checkin
// 			    //       });
					
// 				// alert(checkin);
// 				// };
// 			}
// 		};
// 	}
	
	
 })();
/**
 * Created by IDCS12 on 3/19/2015.
 */
(function(){
    'use strict';
    angular.module('acornApp.hotelSearch.services')
        .factory('hotelSearchService', hotelSearchServiceFun);
        
        
    hotelSearchServiceFun.$inject = ['HttpService'];
    function hotelSearchServiceFun(HttpService){
    	return{
            cities:cities,            
        };

        function cities(){
            return (HttpService.PublicServiceGet('api/city/'));
        }
        
    }
    
})();
/**
 * Created by Innoticalajay on 11/20/2015.
 */
(function(){
    'use strict';

    angular.module('acornApp.preRegistration.controllers')
        .controller('PreRegistrationController',PreRegistrationControllerFun);

    PreRegistrationControllerFun.$inject = ['$scope','$interval', '$stateParams', '$state', '$http', '$timeout', '$filter' , 'notifications', 'preRegistrationService'];

    function PreRegistrationControllerFun($scope, $interval, $stateParams, $state, $http, $timeout, $filter , notifications, preRegistrationService){
        var vm = this;
        //function to submit the form after all validation has occurred
        vm.hotelName = decodeURIComponent($stateParams.hotel_name);
        $scope.checkin_date="";

        vm.preRegistration = {
            hotel_name : vm.hotelName,
            name:"",
            phone_no:null,
            arrival_date:"",
            depart_date:"",
            email:"",
            no_of_adults:null,
            no_of_child:null,
            speacial_request:""
        };
        vm.loading =false;

        vm.submitForm = function(isValid){
            vm.loading =true;
            vm.preRegistration.arrival_date = $scope.checkin_date;
            //check to maake sure the form is completely valid
            //alert("the data is " + JSON.stringify(vm.preRegistration));
            if(isValid){
                //alert("our form is amzing");
                preRegistrationService.preRegistration(vm.preRegistration).success(function(response){
                    alert("Your pre registration request is bring processed and Acorn management department contact you shortly via email on " + vm.preRegistration.email);
                    notifications.showSuccess({message: 'Your request has been send to Acorn Team.'});
                    $state.go('home');
                    vm.loading =false;
                }).error(function(error){
                    notifications.showError({message: 'Failed to send your request. Please try again after 2 minutes'});
                    vm.loading =false;
                });
            }
        };
    }
})();
/**
 * Created by Innoticalajay on 11/20/2015.
 */

/**
 * Created by Innoticalajay on 11/20/2015.
 */
(function(){
    'use strict';

    angular.module('acornApp.preRegistration.services')
        .factory('preRegistrationService', preRegistrationServiceFun);

    preRegistrationServiceFun.$inject = ['HttpService'];

    function preRegistrationServiceFun(HttpService){
        return{
            preRegistration : preRegistrationFun
        };

        function preRegistrationFun(data){
            return (HttpService.PublicServicePost('api/tempbooking/', data));
        }
    }
})();



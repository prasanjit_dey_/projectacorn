(function (){
    'use strict';

    angular.module('acornApp.HttpService',[])
        .service('HttpService', HttpServiceFun);
        
    HttpServiceFun.$inject=['$http','$rootScope'];
       
    function HttpServiceFun($http, $rootScope){
        return{
              PrivateServiceGet: PrivateServiceGetFun,
              PublicServiceGet: PublicServiceGetFun,
              publicServiceGetwithString:publicServiceGetwithStringFun,
              PrivateServicePost:PrivateServicePostFun,
              PublicServicePost:PublicServicePostFun
        };

        function PrivateServiceGetFun(url_){
             return $http({
                url : $rootScope.basePath + url_,
                method: 'GET',
                headers : {
                     'Content-Type' : 'application/json'
                }
            });

        }
        function PublicServiceGetFun(url_){
            return $http({
                url :  $rootScope.basePath +url_,
                method: 'GET',
                headers : {
                     'Content-Type' : 'application/json'

                }
            });
        }
        function publicServiceGetwithStringFun(url_ , data_){
            return $http({
                url :  $rootScope.basePath +url_,
                method: 'GET',
                data: data_,
                headers : {
                    'Content-Type' : 'application/json'

                }
            });
        }
        function PrivateServicePostFun(url_, data_){
             return $http({
                url : $rootScope.basePath + url_,
                method: 'POST',
                data: data_,
                headers : {
                     'Content-Type' : 'application/json'
                }
            });
        }
        function PublicServicePostFun(url_, data_){
              return $http({
                url : $rootScope.basePath + url_,
                method: 'POST',
                data: data_,
                headers : {
                     'Content-Type' : 'application/json'
                }
            });
        }

    }


})();

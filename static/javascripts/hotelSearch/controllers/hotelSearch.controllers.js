/**
 * Created by IDCS12 on 3/18/2015.
 */
(function(){
    "use strict";
    angular.module('acornApp.hotelSearch.controllers')
        .controller('HotelSearchController', HotelSearchControllerFun);


    HotelSearchControllerFun.$inject = ['$scope','$http', '$interval', 'hotelSearchService'];
    

    function HotelSearchControllerFun($scope, $http, $interval, hotelSearchService){
        var vm=this;
		// alert("HotelSearch calls");
    	/*$http.get($scope.static_path+"http://127.0.0.1:8000/api/searchpage/").success(function(data){
          $scope.data= data;
         });*/
         
        vm.cities=[];

        hotelSearchService.cities().success(function(response){
            //alert("success");
            vm.cities = response;
            vm.citySeleted='';
            console.log(JSON.stringify(response));
        }).error(function(){
            // alert("error");
        });
        

        
        $scope.closee=function(){
                $('.modal-backdrop').css("position", "static");
        };

         
    }

})();


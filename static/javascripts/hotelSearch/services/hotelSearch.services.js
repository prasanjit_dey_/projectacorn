/**
 * Created by IDCS12 on 3/19/2015.
 */
(function(){
    'use strict';
    angular.module('acornApp.hotelSearch.services')
        .factory('hotelSearchService', hotelSearchServiceFun);
        
        
    hotelSearchServiceFun.$inject = ['HttpService'];
    function hotelSearchServiceFun(HttpService){
    	return{
            cities:cities,            
        };

        function cities(){
            return (HttpService.PublicServiceGet('api/city/'));
        }
        
    }
    
})();
/**
 * Created by Innoticalajay on 10/19/2015.
 */
(function(){
    "use strict";

    angular.module('acornApp.aboutUs.controllers')
        .controller('AboutUsController', AboutUsControllerFun);

    AboutUsControllerFun.$inject=['$scope', '$timeout'];

    function AboutUsControllerFun($scope, $timeout){
        var vm = this;
        vm.contactUsTabClick = function(){
            $timeout(function(){
                $(".modal-backdrop").css("z-index", "1");
            });
        };
    }
})();
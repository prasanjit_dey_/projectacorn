/**
 * Created by Innoticalajay on 10/19/2015.
 */
(function(){
    "use strict";

    angular.module('acornApp.aboutUs',[
        'acornApp.aboutUs.controllers',
        'acornApp.aboutUs.directives',
        'acornApp.aboutUs.services'
    ]);

    angular.module('acornApp.aboutUs.controllers',[]);
    angular.module('acornApp.aboutUs.directives',[]);
    angular.module('acornApp.aboutUs.services',[]);
})();
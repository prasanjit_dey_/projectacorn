(function () {
    'use strict';

    var app=angular
        .module('acornApp', [
            'acornApp.config',
            //'ngAnimate',
            'acornApp.routes',
            'acornApp.home',
            'acornApp.hotelSearch',
            'acornApp.hotels',
            'acornApp.hotel',
            'acornApp.HttpService',
            'acornApp.aboutUs',
            'acornApp.preRegistration',
            'ngSanitize',
            'ui.select',
            'ui.bootstrap',
            'ngNotificationsBar'
        ]);

    angular.module('acornApp.config', []);

    app.config(['notificationsConfigProvider', function (notificationsConfigProvider) {
        // auto hide
        notificationsConfigProvider.setAutoHide(true);

        // delay before hide
        notificationsConfigProvider.setHideDelay(3000);
        // Set an animation for hiding the notification
        //notificationsConfigProvider.setAutoHideAnimation('fadeOutNotifications');
    }]);


    /*angular.module('acornApp.HttpService', []);*/




    // Module specific configuration
    /*****************base path use for api hitting bars url ***************     /*angular.module('acornApp.config')
     .value('acornApp.config', {

     // basePath: 'http://52.74.163.60/' // Set your base path here
     });*/




     angular.module('acornApp.routes', ['ngRoute', 'ui.router', 'base64']).run(['$rootScope', function($rootScope) {
        /*$rootScope.static_path = 'https://healthbrio.s3.amazonaws.com/';*/
         $rootScope.static_path = 'http://acornhotels.com/static/';
         $rootScope.basePath = 'http://acornhotels.com/';
        /*$rootScope.static_path = 'http://192.168.56.1:8000/static/';*/
        /*$rootScope.basePath = 'http://127.0.0.1:8000/';*/

        $rootScope.$on('$stateChangeSuccess',
            function(event, toState, toParams, fromState, fromParams){
                // alert(toState.name);
                document.body.scrollTop = document.documentElement.scrollTop = 0;
                if(toState.name==='/'){
                    $('html,Body').css({
                        'overflow' : 'hidden',
                        'height' : '100%'
                    });
                }else{
                    $('Body').css({
                        'overflow' : 'scroll',
                        'height' : '100%'
                    });
                }

            });
        }]);




})();

/**
 * Created by IDCS12 on 3/19/2015.
 */
(function(){
    'use strict';
    angular.module('acornApp.home.services')
        .service('homeService', homeServiceFun);
        
        homeServiceFun.$inject = ['HttpService'];

        function homeServiceFun(HttpService){
            return{
                cities:cities
            };
            function cities(){
                return (HttpService.PublicServiceGet('api/city/'));
            }
             /*this.getdata = function(){
                    
                    return HttpService.PublicServiceGet('api/homeslider/');
                }*/
        }

})();
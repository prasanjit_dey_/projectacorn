/**
 * Created by IDCS12 on 3/19/2015.
 */
(function(){
	'use strict';
	
    angular.module('acornApp.home.directives')
	.directive('logoSlider', logoSliderFun);

	angular.module('acornApp.home.directives')
	.directive('onePageScroll', onePageScrollFun);

    angular.module('acornApp.home.directives')
	.directive('bootselectpicker', bootselectpickerFun);

    angular.module('acornApp.home.directives')
        .directive("dateTimeIn",dateTimeInFun);

    angular.module('acornApp.home.directives')
        .directive("dateTimeInMob",dateTimeInMobFun);

	logoSliderFun.$inject=[];
	onePageScrollFun.$inject=[];
    bootselectpickerFun.$inject=['$timeout'];
    dateTimeInFun.$inject = ['$timeout'];
    dateTimeInMobFun.$inject = ['$timeout'];

	function logoSliderFun(){
		return{
			restrict : 'AE',			
			link : function(scope, element, attrs){
				//alert("Slides");
				// $(element).bxSlider({
				// 	speed: 6000
				// });
				
				// $('#slider').bxSlider({
				// 	maxSlides: 4,
				// 	slideWidth: 170,
				// 	slideMargin: 10,
				// 	ticker: true,
				// 	speed: 6000
				// 	});	
			}
		};
	}
		
	function onePageScrollFun(){
		return{
			restrict:'AE',
			link : function(scope,elements,attrs){
	
				$(".main").onepage_scroll({
			        sectionContainer: "section",
			        animationTime: 2000,
			        responsiveFallback: false,
			        loop: true,
			        pagination: true,
			        updateURL: true, 
			        direction: "vertical",
			        beforeMove: function(index) 
			        {
			            //$(".main").moveTo(2);
			            // alert(index.length.value)
			        }
			      });
			}
		};
	}

    function bootselectpickerFun($timeout){
        return{
            restrict :'AE',
            link : function(scope, element, attrs){
                //alert("sdfds");
                $timeout(function() {
                  /*element.selectpicker();*/

                    $('.selectpicker').selectpicker();
               });

            }
        };
    }





    function dateTimeInFun($timeout){
        //alert("Date Time Directive");
        return{
            restrict: 'AE',

            link: function(scope,ele,attr){
                $("#checkin").datepicker({
                    dateFormat : "yy-mm-dd",
                    minDate:0,
                    changeMonth: true,
                    changeYear: true,
                    onSelect: function(date) {
                        //  alert(date);
                        $("#checkout").datepicker("destroy");
                        var checkindate = document.getElementById("checkin").value;

                        scope.$apply(function(){
                            scope.checkin_date= checkindate;
                        });
                        console.log(checkindate);

                        $('#checkout').datepicker({
                            minDate :  checkindate,
                            dateFormat : 'yy-mm-dd',
                            changeMonth: true,
                            changeYear: true,
                            });
                    }
                });
            }
        };
    }
    function dateTimeInMobFun($timeout){
        //alert("Date Time Directive");
        return{
            restrict: 'AE',
            link: function(scope,ele,attr){
                $("#checkinmob").datepicker({
                    dateFormat : "yy-mm-dd",
                    minDate:0,
                    changeMonth: true,
                    changeYear: true,
                    onSelect: function(date) {
                        //  alert(date);
                        $("#checkoutmob").datepicker("destroy");
                        var checkindate = document.getElementById("checkinmob").value;
                        scope.$apply(function(){
                            scope.checkin_date= checkindate;
                        });
                        console.log(checkindate);

                        $('#checkoutmob').datepicker({
                            minDate :  checkindate,
                            dateFormat : 'yy-mm-dd',
                            changeMonth: true,
                            changeYear: true,
                            });
                    }
                });
            }
        };
    }
	
	
})();
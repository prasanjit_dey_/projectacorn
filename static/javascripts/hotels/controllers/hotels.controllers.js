/**
 * Created by IDCS12 on 9/24/2015.
 */
(function(){
    "use strict";
    angular.module('acornApp.hotels.controllers')
        .controller('HotelsController', HotelsControllerFun);

    HotelsControllerFun.$inject = ['$scope', '$interval', '$stateParams', 'hotelsService', '$http', '$timeout', '$filter' , 'notifications'];
    

    function HotelsControllerFun($scope, $interval, $stateParams, hotelsService, $http, $timeout,$filter, notifications){
        var vm=this;
		vm.hotelsList = [];
        vm.cities=[];
        vm.previouscity = $stateParams.hotel_city;
        vm.previouscityId=$stateParams.hotel_city_id;
        $scope.checkin_date = decodeURIComponent($stateParams.checkin);
        vm.checkout_date = decodeURIComponent($stateParams.checkout);
        //alert("after decode" + $scope.checkin_date +" "+ vm.checkout_date );
        //var newdate = vm.checkin_date.split("/").reverse().join("-");
        //vm.checkinDate_ = $filter('date')(new Date(Date.parse($scope.checkin_date)), 'yyyy-MM-dd');
        //vm.checkoutDate_ = $filter('date')(new Date(Date.parse(vm.checkout_date)), 'yyyy-MM-dd');
        //alert("after parsing " + vm.checkinDate_ + " " + vm.checkoutDate_);
        //alert(new Date(Date.parse(vm.checkin_date)).getMonth()+"again decode" +$filter('date')(vm.checkin_date, "yyyy-MM-dd"));
        vm.nights ='';
        /**************night caluclation *********************/
         if ($scope.checkin_date && vm.checkout_date) {
            var difference_msec = Date.parse(vm.checkout_date) - Date.parse($scope.checkin_date) ;
            vm.nights=  difference_msec / 86400000;
        }
        //alert($stateParams.hotel_city + vm.checkin_date + vm.checkout_date);

        vm.citySeleted={
            id:$stateParams.hotel_city_id,
            city_name:$stateParams.hotel_city
        };

        vm.sortby_data=[];
        vm.selectedSortData='';

        /*getting city data */
        hotelsService.searchPageMisc().success(function(response){
           //alert("success");
            vm.cities = response.city_data;
            console.log("cities"+ response.city_data);
            for(var i =0; i<response.city_data.length;i++){
                if(response.city_data[i].id == vm.previouscityId){
                    //alert("city id found" +response.city_data[i].id);
                    //vm.citySeleted.id = response.city_data[i].id;
                    //vm.citySeleted.city_name = response.city_data[i].city_name;
                    break;
                }else{
                   //console.log("City Name not found from the api ")
                }
            }
            vm.sortby_data=response.sortby_data;
            vm.selectedSortData=response.sortby_data[0];
            console.log(JSON.stringify(response.sortby_data));

        }).error(function(){
            notifications.showError({message: 'Oh snap ! city selection connection Lost!!!'});
        });
        //alert("id is" +vm.previouscityId);

        vm.cityFilterData = {
            city : vm.previouscityId,
            min_price :0,
            max_price :5000,
            check_in:$scope.checkin_date,
            check_out:vm.checkout_date,
            sortby:'rating'
        };



        /*getting all hotel list data*/
        /*//city=2&min_price=0&max_price=1800&check_in=2015-01-01&check_out=2015-01-01&sortby=rating*/
        hotelsService.allHotels(vm.cityFilterData).success(function(response){
            //alert("success");
            vm.hotelsList = response;
            //alert(JSON.stringify(response));
        }).error(function(error){
            notifications.showError({message: 'Oh snap ! hotels list connection Lost!!!'});
        });
        vm.changeSearchData = function(city,checkin, checkout){
             //console.log("this function ality need to be done" + city + checkin + checkout);

            /*changing that city selected is */
            if(typeof(city)=='object'){
                vm.cityFilterData = {
                    city : city.id,
                    min_price :0,
                    max_price :5000,
                    check_in:checkin,
                    check_out:checkout,
                    sortby:'rating'
                };
                hotelsService.allHotels(vm.cityFilterData).success(function(response){
                    vm.hotelsList = response;
                }).error(function(error){
                    notifications.showError({message: 'Oh snap ! hotels list connection Lost!!!'});
                });
            }else{
                vm.cityFilterData = {
                    city : vm.previouscityId,
                    min_price :0,
                    max_price :5000,
                    check_in:checkin,
                    check_out:checkout,
                    sortby:'rating'
                };
                hotelsService.allHotels(vm.cityFilterData).success(function(response){
                    vm.hotelsList = response;
                }).error(function(error){
                    notifications.showError({message: 'Oh snap ! hotels list connection Lost!!!'});
                });
            }
            if (checkin && checkout) {
                var difference_msec = Date.parse(checkout) - Date.parse(checkin) ;
                vm.nights=  difference_msec / 86400000;
            }
           // alert(JSON.stringify(vm.citySeleted));
            /*alert(JSON.stringify(city.city_name) +" " +vm.checkin_date+" "+checkout );
            //console.log(vm.selectedCity.city_name + ' '+ vm.selectedCity.id + ' '+vm.checkin_date +' '+ vm.checkout_date);
            vm.cityFilterData = {
                city :2,
                min_price :0,
                max_price :2000,
                check_in:2015-01-01,
                check_out:2015-01-01,
                sortby:'rating'
            };
            hotelsService.allHotels(vm.cityFilterData).success(function(response){
                //alert("success");
                vm.hotelsList = response;
                console.log(response);
            }).error(function(error){
                alert("error");
            });*/
        };

        vm.contactUsTabClick = function(){
            $timeout(function(){
                $(".modal-backdrop").css("z-index", "0");
            });
        };
    }

})();

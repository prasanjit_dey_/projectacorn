/**
 * Created by IDCS12 on 3/24/2015.
 */
(function(){
	'use strict';

    angular.module('acornApp.hotels.directives',[])
        .directive('myMaps', myMapsFun);



    myMapsFun.$inejct=[];


    function myMapsFun(){
        return{
            restrict:'E',
            template :'<div></div>',
            replace:'true',
            link:function(scope,element,attrs){
                //alert(scope.$eval(attrs.latitude));
                var latitude =scope.$eval(attrs.latitude);
                var longitude =scope.$eval(attrs.longitude);
                var myLatLng =  new google.maps.LatLng(latitude,longitude);
                var mapOptions = {
                  center:myLatLng,
                  zoom: 8,
                  mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                var map = new google.maps.Map(document.getElementById(attrs.id),mapOptions);
                var marker = new google.maps.Marker({
                      position: myLatLng,
                      map: map,
                      title: 'My Home!'
                  });

                marker.setMap(map);
                marker.setAnimation(google.maps.Animation.BOUNCE);
            }
        };
    }




	
})();
/**
 * Created by IDCS12 on 9/24/2015.
 */
(function(){
    'use strict';
    angular.module('acornApp.hotels.services')
        .factory('hotelsService', hotelsService);
    hotelsService.$inject = ['HttpService'];
    function hotelsService(HttpService){
    	return{
            searchPageMisc:searchPageMiscFun,
            allHotels : allHotels,
            filterHotel : filterHotel
        };

        function searchPageMiscFun(){
            return (HttpService.PublicServiceGet('api/searchpagemisc/'));
        }

        function allHotels(data){
            //city=2&min_price=0&max_price=1800&check_in=2015-01-01&check_out=2015-01-01&sortby=rating
            return (HttpService.publicServiceGetwithString('api/hotel/?city=' +data.city + '&min_price=' + data.min_price + '&max_price=' + data.max_price +'&check_in='+data.check_in+'&check_out='+data.check_out+'&sortby='+data.sortby));
        }

        function filterHotel(){
            return true;
        }

    }
})();
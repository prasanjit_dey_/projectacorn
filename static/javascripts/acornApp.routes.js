(function () {
  'use strict';

  angular
    .module('acornApp.routes')
    .config(indexConfig);

  indexConfig.$inject = ['$stateProvider', '$urlRouterProvider'];

/**
   * @name config
   * @desc Define valid application routes*/

   //***  stateProvider for Symptoms*****
   window.static_path = 'http://acornhotels.com/static/';
  /*window.static_path = 'http://127.0.0.1:8000/static/';*/


  /*window.static_path = 'https://healthbrio.s3.amazonaws.com/';*/

  function indexConfig($stateProvider, $urlRouterProvider){
    $urlRouterProvider.otherwise('/');
    $stateProvider
        /*********home page routing ***********/
        /*.state('home', {
            url: '/home',
            controller:'HomeController',
            controllerAs:'home_vm',
            templateUrl: static_path+'templates/home/home.html'
        })*/
        /**********hotelSearchrouting NEw Pardeep ****************/
        .state('home',{
            url:'/',
            controller:'HomeController',
            controllerAs:'home_vm',
            templateUrl:static_path+'templates/home/home.html'
        })

        /*****************hotels search result list****************/
        .state('hotels',{
            url:'/hotels/:hotel_city_id/:hotel_city/:checkin/:checkout',
            controller:'HotelsController',
            controllerAs:'hotels_vm',
            templateUrl:static_path+'templates/hotels/hotels.html'
        })
        /*****************hotel Detail ***********************/
        .state('hotel',{
            url:'/hotel',
            controller:'HotelController',
            controllerAs:'hotel_vm',
            templateUrl:static_path+'templates/hotel/hotel.html'
        })
        /**********about US Routinf ****************/
        .state('aboutUs',{
            url:'/aboutUs',
            controller : 'AboutUsController',
            controllerAs:'avoutUs_vm',
            /*controller : function($scope, $timeout){
                alert("sdfd");
                $scope.contactUsTabClick = function()
                {
                    alert("sdafds");
                    $timeout(function(){
                        $(".modal-backdrop").css("z-index", "0");
                    });
                };
            },*/
            //controller:'AboutUsController',
            //controllerAs:'aboutUs_vm',
            templateUrl:static_path+'templates/aboutUs/aboutUs.html'
        })
        .state('terms',{
            url:'/terms',
            controller : function($scope, $timeout){
                $scope.contactUsTabClick = function(){
                    $timeout(function(){
                        $(".modal-backdrop").css("z-index", "0");
                    });
                };
            },
            templateUrl:static_path+'templates/terms/terms.html'
        })
        .state('policy',{
            url:'/policy',
            controller : function($scope, $timeout){
                $scope.contactUsTabClick = function(){
                    $timeout(function(){
                        $(".modal-backdrop").css("z-index", "0");
                    });
                };
            },
            //template:'<h1>Policy</h1>'
            templateUrl:static_path+'templates/policy/policy.html'
        })
        .state('cancelation',{
            url:'/cancellationPolicy',
            controller : function($scope, $timeout){
                $scope.contactUsTabClick = function(){
                    $timeout(function(){
                        $(".modal-backdrop").css("z-index", "0");
                    });
                };
            },
            //template:'<h1>Policy</h1>'
            templateUrl:static_path+'templates/cancelation/cancelation.html'
        })
        .state('preregistration',{
            url:'/pre-registration/:hotel_name',
            controller:'PreRegistrationController',
            controllerAs:'preRegistration_vm',
            templateUrl:static_path +'templates/preregistration/preregistration.html'
        });

  }
})();
/**
 * Created by Innoticalajay on 11/20/2015.
 */
(function(){
    'use strict';

    angular.module('acornApp.preRegistration',[
        'acornApp.preRegistration.controllers',
        'acornApp.preRegistration.directives',
        'acornApp.preRegistration.services',
    ]);

    angular.module('acornApp.preRegistration.controllers',[]);
    angular.module('acornApp.preRegistration.directives',[]);
    angular.module('acornApp.preRegistration.services',[]);

})();
/**
 * Created by Innoticalajay on 11/20/2015.
 */
(function(){
    'use strict';

    angular.module('acornApp.preRegistration.services')
        .factory('preRegistrationService', preRegistrationServiceFun);

    preRegistrationServiceFun.$inject = ['HttpService'];

    function preRegistrationServiceFun(HttpService){
        return{
            preRegistration : preRegistrationFun
        };

        function preRegistrationFun(data){
            return (HttpService.PublicServicePost('api/tempbooking/', data));
        }
    }
})();



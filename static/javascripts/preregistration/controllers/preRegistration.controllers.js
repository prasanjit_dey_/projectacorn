/**
 * Created by Innoticalajay on 11/20/2015.
 */
(function(){
    'use strict';

    angular.module('acornApp.preRegistration.controllers')
        .controller('PreRegistrationController',PreRegistrationControllerFun);

    PreRegistrationControllerFun.$inject = ['$scope','$interval', '$stateParams', '$state', '$http', '$timeout', '$filter' , 'notifications', 'preRegistrationService'];

    function PreRegistrationControllerFun($scope, $interval, $stateParams, $state, $http, $timeout, $filter , notifications, preRegistrationService){
        var vm = this;
        //function to submit the form after all validation has occurred
        vm.hotelName = decodeURIComponent($stateParams.hotel_name);
        $scope.checkin_date="";

        vm.preRegistration = {
            hotel_name : vm.hotelName,
            name:"",
            phone_no:null,
            arrival_date:"",
            depart_date:"",
            email:"",
            no_of_adults:null,
            no_of_child:null,
            speacial_request:""
        };
        vm.loading =false;

        vm.submitForm = function(isValid){
            vm.loading =true;
            vm.preRegistration.arrival_date = $scope.checkin_date;
            //check to maake sure the form is completely valid
            //alert("the data is " + JSON.stringify(vm.preRegistration));
            if(isValid){
                //alert("our form is amzing");
                preRegistrationService.preRegistration(vm.preRegistration).success(function(response){
                    alert("Your pre registration request is bring processed and Acorn management department contact you shortly via email on " + vm.preRegistration.email);
                    notifications.showSuccess({message: 'Your request has been send to Acorn Team.'});
                    $state.go('home');
                    vm.loading =false;
                }).error(function(error){
                    notifications.showError({message: 'Failed to send your request. Please try again after 2 minutes'});
                    vm.loading =false;
                });
            }
        };
    }
})();
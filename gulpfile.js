/**
 * Created by Innoticalajay on 10/14/2015.
 */
var gulp = require('gulp');

// Include Our Plugins
var jshint = require('gulp-jshint');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var minifyCss = require("gulp-minify-css");
var csslint = require('gulp-csslint');





//lint task for errer checking in js file
gulp.task('lint', function() {
    return gulp.src('static/javascripts/**/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});


gulp.task('lintselect', function(){
   return gulp.src('static/lib/js/select.js')
       .pipe(jshint())
       .pipe(jshint.reporter('default'));
});
gulp.task('lintbxslider', function(){
    return gulp.src('static/lib/js/boxslider/jquery.bxSlider.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});
/*gulp.task('lintCalender', function(){
    return gulp.src('static/lib/js/calender*//*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});*/
/***********for error checking  in the css file ********************/
/*gulp.task('style',function(){
   gulp.src('static/lib/css*//*.css')
       .pipe(csslint())
       .pipe(csslint.reporter());
});*/
/*gulp.task('calender',function(){
    gulp.src('static/lib/css/datepicker/angular-datepicker.css')
        .pipe(csslint())
        .pipe(csslint.reporter());
});*/

// Compile Our Sass No Need On this project[acorn hotels]

/*gulp.task('sass', function() {
    return gulp.src('scss*//*.scss')
        .pipe(sass())
        .pipe(gulp.dest('css'));
});*/


// Concatenate & Minify JS
//gulp.task('scripts', function() {
//    return gulp.src('static/lib/js/**/*.js')
//        .pipe(concat('all.js'))
//        .pipe(gulp.dest('static/lib/js/dist'))
//        .pipe(rename('all.min.js'))
//        .pipe(uglify())
//        .pipe(gulp.dest('static/lib/js/dist'));
//});




gulp.task('javascripts', function() {
    return gulp.src('static/javascripts/**/*.js')
        .pipe(concat('main.js'))
        .pipe(gulp.dest('static/dist'))
        .pipe(rename('main.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('static/dist'));
});
gulp.task('select', function(){
    return gulp.src('static/lib/js/select.js')
        .pipe(concat('select.js'))
        .pipe(gulp.dest('static/lib/js/select/dist/'))
        .pipe(rename('select.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('static/lib/js/select/dist/'));
});
gulp.task('bxslider', function(){
    return gulp.src('static/lib/js/boxslider/jquery.bxSlider.js')
        .pipe(concat('jquery.bxSlider.js'))
        .pipe(gulp.dest('static/lib/js/boxslider/dist/'))
        .pipe(rename('jquery.bxSlider.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('static/lib/js/boxslider/dist/'));
});
/*gulp.task('calender', function(){
    return gulp.src('static/lib/js/calender*//*.js')
        .pipe(concat('calender.js'))
        .pipe(gulp.dest('static/lib/js/calender/dist/'))
        .pipe(rename('calender.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('static/lib/js/calender/dist/'));
});*/

gulp.task('minify-css', function(){
    gulp.src('static/lib/css/custom/*.css')
        .pipe(concat('style.css'))
        .pipe(gulp.dest('static/lib/css/dist'))
        .pipe(rename('style.min.css'))
        .pipe(minifyCss())
        .pipe(gulp.dest('static/lib/css/dist'));
});
/*gulp.task('minify-datepicker', function(){
    gulp.src('static/lib/css/datepicker/angular-datepicker.css')
        .pipe(concat('angular-datepicker.css'))
        .pipe(gulp.dest('static/lib/css/datepicker/dist'))
        .pipe(rename('angular-datepicker.min.css'))
        .pipe(minifyCss())
        .pipe(gulp.dest('static/lib/css/datepicker/dist'));
});*/
gulp.task('minify-calender', function(){
   gulp.src('static/lib/css/calender/calender.css')
       .pipe(rename('calender.min.css'))
       .pipe(minifyCss())
       .pipe(gulp.dest('static/lib/css/calender/'));
});

gulp.task('minify-fancybox', function(){
    gulp.src('static/lib/js/fb-plugin/source/jquery.fancybox.css')
        .pipe(rename('jquery.fancybox.min.css'))
        .pipe(minifyCss())
        .pipe(gulp.dest('static/lib/js/fb-plugin/source/dist'));
});

gulp.task('minify-loader', function(){
   gulp.src('static/lib/css/loader/whirly.css')
       .pipe(rename('whirly.min.css'))
       .pipe(minifyCss())
       .pipe(gulp.dest('static/lib/css/loader/'))
});

// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch('static/javascripts/**/*.js', ['lint', 'lintselect', 'javascripts','select', 'lintbxslider', 'bxslider', 'minify-css', 'minify-calender', 'minify-fancybox']);
    //gulp.watch('scss.scss', ['sass']);

});

gulp.task('default', ['lint', 'lintselect', 'javascripts','select', 'lintbxslider', 'bxslider', 'minify-css', 'minify-calender', 'minify-fancybox', 'minify-loader','watch']);

/*
gulp.task('default', function(){
   console.log("gulp install");
});*/

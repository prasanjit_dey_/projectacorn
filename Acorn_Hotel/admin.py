from django.contrib import admin
from Acorn_Hotel.models import *
from django import forms
from django.db.models import Q
from django import forms
# Register your models here.

class HomePage_SliderForm(forms.ModelForm): 

	class Meta:
		model =	 HomePage_Slider
		fields = '__all__'

	def clean(self):
		images_len = len(self.cleaned_data.get('slider_image'))
		print "images",images_len
		if images_len < 3 or images_len > 5 :
			print "inside if "
			raise forms.ValidationError("no of images should be less than 6 or greater tha 2")
		return self.cleaned_data

class HomePage_SliderCustom(admin.ModelAdmin):
	filter_horizontal = ('slider_image',)
	readonly_fields=('slider_name',)
	fieldsets=[
		('Home Slider Details', {'fields': ['slider_name','slider_heading','slider_detail','slider_image','slider_position','is_active' ]})
	]
	form = HomePage_SliderForm
	def has_add_permission(self, request):
	 	return False
	
class Search_PageCustom(admin.ModelAdmin):
	
	def has_add_permission(self, request):
		return False

class HotelCustom(admin.ModelAdmin):
	# list_display=('slider_name','address','phone_number','rent')
	
	# readonly_fields=('hotel_rating',)
	fieldsets=[
		('Home Slider Basic Details', {'fields': ['hotel_name','hotel_tag_line','hotel_description','hotel_street_address','city' ,'pin_code','hotel_longitude' ,'hotel_latiitude','hotel_facilities' ]}),
		('Home Slider Image Details', {'fields': ['hotel_logo','default_image' ,'hotel_images']}),
		('Home Slider Other Details', {'fields': ['forward_link' ,'hotel_rating','is_active']}),
	]
	

admin.site.register(Hotel_Images)
admin.site.register(HomePage_Slider,HomePage_SliderCustom)
admin.site.register(Country)
admin.site.register(City)
admin.site.register(Search_Page,Search_PageCustom)
admin.site.register(Facility)

admin.site.register(Hotel,HotelCustom)

admin.site.register(Room)
admin.site.register(Price_List)
admin.site.register(SortBy_List)
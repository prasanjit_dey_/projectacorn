from django.shortcuts import render
from rest_framework import viewsets,mixins
from Acorn_Hotel.models import *
from Acorn_Hotel.serializers import *
from rest_framework.response import Response
from django.db.models import Q
from django.core.mail import send_mail
from django.template.loader import render_to_string, get_template
from django.core.mail import EmailMessage
from django.template import Context
import datetime
import json
# Create your views here.

class HomePage_Slider_Viewset(viewsets.ReadOnlyModelViewSet):
	
	queryset = HomePage_Slider.objects.filter(is_active = True)
	serializer_class = HomePage_SliderSerializer

class City_Viewset(viewsets.ReadOnlyModelViewSet):
	
	queryset = City.objects.filter(is_active = True)
	serializer_class = CitySerializer	

class Search_Page_Viewset(viewsets.ReadOnlyModelViewSet):
	
	queryset = Search_Page.objects.filter(is_active = True)
	serializer_class = Search_PageSerializer	

class Facility_Viewset(viewsets.ReadOnlyModelViewSet):
	
	queryset = Facility.objects.all()
	serializer_class = FacilitySerializer	

class Hotel_Viewset(viewsets.ReadOnlyModelViewSet):
	
	queryset = Hotel.objects.filter(is_active = True)
	serializer_class = HotelSerializer	

	def list(self, request):
		data = request.GET
		city = data.get('city')
		print "city",city
		min_price = data.get('min_price')
		print "min_price",min_price
		max_price = data.get('max_price') 
		print "max_price",max_price
		in_date = data.get('check_in')
		out_date = data.get('check_out')
		sortby = data.get('sortby')
		hotel_json = []
		all_hotel = Hotel.objects.filter(Q(is_active = True) & Q(city__id = city ))
		if sortby == 'rating':
			print 'inside if'
			all_hotel = all_hotel.order_by('hotel_rating')

		print "all_hotel",all_hotel
		for hotel in all_hotel:
			all_room = Room.objects.filter(Q(is_active = True) & Q(hotel_name = hotel) & Q(room_price__lte = max_price) & Q(room_price__gte = min_price) & (Q(check_out__lte = in_date) | Q(check_out = None) ))
			if all_room.exists() :
				serializer = HotelSerializer(hotel)
				hotel_json = hotel_json + [serializer.data]

		return Response(hotel_json)

class HomePage_Slider_Viewset(viewsets.ModelViewSet):

    queryset = HomePage_Slider.objects.filter(is_active = True)
    serializer_class = HomePage_SliderSerializer

class Search_Page_MiscViewset(viewsets.ReadOnlyModelViewSet):
	queryset = SortBy_List.objects.all()
	serializer_class = SortBy_ListSerializer	


	def list(self, request):
		searched_result = {}
		
		serializer = SortBy_List.objects.filter(is_active = True)
		sortby_data = SortBy_ListSerializer(serializer,many = True)


		serializer = Price_List.objects.filter(is_active = True)
		price_list_data = Price_ListSerializer(serializer, many = True)

		serializer = City.objects.filter(is_active = True)
		city_data = CitySerializer(serializer, many = True)

		searched_result = {'city_data':city_data.data, 'price_list_data':price_list_data.data, 'sortby_data':sortby_data.data}

		return Response(searched_result)

class Room_Viewset(viewsets.ReadOnlyModelViewSet):
	
	queryset = Room.objects.all()
	serializer_class = RoomSerializer	

# class Price_ListViewset(viewsets.ReadOnlyModelViewSet):
	
# 	queryset = Price_List.objects.all()
# 	serializer_class = Price_ListSerializer	

# class SortBy_ListViewset(viewsets.ReadOnlyModelViewSet):
	
# 	queryset = SortBy_List.objects.all()
# 	serializer_class = SortBy_ListSerializer	
class TempBookingViewSet(mixins.CreateModelMixin,viewsets.GenericViewSet):

	queryset = Room.objects.all()
	serializer_class = RoomSerializer	
	# http_method_names = ['get','post']

	def create(self, request):


		# 
		data = json.loads(request.body)
		# print data
		# data = json.loads(request.POST["_content"])
		print data
		# return Response (request.POST)
		# print "hhoott",request.POST['hotel_name']
		hotel_name = data['hotel_name']
		# return Response ["hotel_name"+str(hotel_name)]
		print "hotel_name",hotel_name
		person_name = data['name']
		on_date = datetime.date.today()
		email = data['email']
		phone_no = data['phone_no']
		arrival_date = data['arrival_date']
		depart_date = data['depart_date']
		no_of_child = data['no_of_child']
		no_of_adult = data['no_of_adults']
		speacial_request = data['speacial_request']

	
		subject = 'Booking done for '+hotel_name+' on '+unicode(on_date)+' through Pre-Registration'
		to1 = ['reservations@acornhotels.com']
		#to1 = ['ajaykant@innotical.com']
		to2 = ['acornhotels@gmail.com']
		from_email = 'acornhotels@gmail.com'


		ctx = {
			'hotel_name' : hotel_name,
			'name': person_name,
            'email':email,
			'phone_no':phone_no,
			'arrival_date':arrival_date,
			'depart_date': depart_date,
			'no_of_adult':no_of_adult,
            'no_of_child':no_of_child,
			'speacial_request':speacial_request
		}

		message = render_to_string('mail_template/mail.txt', ctx)

		EmailMessage(subject, message, to=to1, from_email=from_email).send()
		EmailMessage(subject, message, to=to2, from_email=from_email).send()

		return Response('email_one')



from django.conf.urls import include, url
from django.contrib import admin
from Acorn_Hotel import views
from rest_framework.routers import DefaultRouter


# ... your normal urlpatterns here



router = DefaultRouter()
router.register(r'homeslider', views.HomePage_Slider_Viewset)
router.register(r'city', views.City_Viewset)
router.register(r'searchpage', views.Hotel_Viewset)
router.register(r'facility', views.Facility_Viewset)
router.register(r'hotel', views.Hotel_Viewset)
router.register(r'room', views.Room_Viewset)
router.register(r'searchpagemisc', views.Search_Page_MiscViewset)
router.register(r'tempbooking', views.TempBookingViewSet)

urlpatterns = [
    url(r'^api/',include(router.urls)),
   
]




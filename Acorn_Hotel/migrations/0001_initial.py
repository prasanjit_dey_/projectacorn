# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('city_name', models.CharField(max_length=50)),
                ('is_active', models.BooleanField(default=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('country_name', models.CharField(max_length=50)),
                ('is_active', models.BooleanField(default=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Facility',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('facility_name', models.CharField(max_length=100)),
                ('slug', models.CharField(max_length=101, null=True, blank=True)),
                ('is_active', models.BooleanField(default=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='HomePage_Slider',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('slider_name', models.CharField(max_length=100, null=True, blank=True)),
                ('slider_heading', models.CharField(max_length=100, null=True, blank=True)),
                ('slider_detail', models.TextField(null=True, blank=True)),
                ('slider_position', models.PositiveIntegerField(default=0)),
                ('is_active', models.BooleanField(default=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Hotel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('hotel_name', models.CharField(max_length=200)),
                ('hotel_tag_line', models.CharField(max_length=200, null=True, blank=True)),
                ('hotel_street_address', models.CharField(max_length=300, null=True, blank=True)),
                ('pin_code', models.PositiveIntegerField(default=0, max_length=6, null=True, blank=True)),
                ('hotel_description', models.TextField(null=True, blank=True)),
                ('hotel_longitude', models.CharField(max_length=100, null=True, blank=True)),
                ('hotel_latiitude', models.CharField(max_length=100, null=True, blank=True)),
                ('hotel_rating', models.CharField(default=b'2', max_length=8, choices=[(b'2', b'2'), (b'3', b'3'), (b'4', b'4'), (b'5', b'5')])),
                ('forward_link', models.CharField(max_length=300, null=True, blank=True)),
                ('is_active', models.BooleanField(default=False)),
                ('city', models.ForeignKey(blank=True, to='Acorn_Hotel.City', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Hotel_Images',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image_name', models.CharField(max_length=100, null=True, blank=True)),
                ('image_heading', models.CharField(max_length=100, null=True, blank=True)),
                ('image_detail', models.TextField(null=True, blank=True)),
                ('image_url', models.ImageField(null=True, upload_to=b'images', blank=True)),
                ('is_active', models.BooleanField(default=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Price_List',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('min_price', models.PositiveIntegerField(default=0, max_length=10)),
                ('max_price', models.PositiveIntegerField(default=0, max_length=10)),
                ('is_active', models.BooleanField(default=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Room',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('room_name', models.CharField(max_length=30)),
                ('room_price', models.CharField(default=b'0', max_length=10)),
                ('check_in', models.DateField(null=True, blank=True)),
                ('check_out', models.DateField(null=True, blank=True)),
                ('is_active', models.BooleanField(default=False)),
                ('amenities', models.ManyToManyField(to='Acorn_Hotel.Facility', null=True, blank=True)),
                ('hotel_name', models.ForeignKey(blank=True, to='Acorn_Hotel.Hotel', null=True)),
                ('room_images', models.ManyToManyField(to='Acorn_Hotel.Hotel_Images', null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Search_Page',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('searchPage_Heading', models.CharField(max_length=150, null=True, blank=True)),
                ('searchPage_Detail', models.TextField(null=True, blank=True)),
                ('is_active', models.BooleanField(default=False)),
                ('main_image', models.ForeignKey(related_name='hotel_images', blank=True, to='Acorn_Hotel.Hotel_Images', null=True)),
                ('nearby_Place_images', models.ManyToManyField(related_name='nearby_places_images', null=True, to='Acorn_Hotel.Hotel_Images', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SortBy_List',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('sort_by_category_name', models.CharField(max_length=50)),
                ('is_active', models.BooleanField(default=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='hotel',
            name='default_image',
            field=models.ForeignKey(related_name='default_image', blank=True, to='Acorn_Hotel.Hotel_Images', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='hotel',
            name='hotel_facilities',
            field=models.ManyToManyField(to='Acorn_Hotel.Facility', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='hotel',
            name='hotel_images',
            field=models.ManyToManyField(related_name='slider_images', null=True, to='Acorn_Hotel.Hotel_Images', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='hotel',
            name='hotel_logo',
            field=models.ForeignKey(related_name='hotel_logo', blank=True, to='Acorn_Hotel.Hotel_Images', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='homepage_slider',
            name='slider_image',
            field=models.ManyToManyField(to='Acorn_Hotel.Hotel_Images', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='facility',
            name='facility_image',
            field=models.ForeignKey(blank=True, to='Acorn_Hotel.Hotel_Images', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='city',
            name='country_name',
            field=models.ForeignKey(blank=True, to='Acorn_Hotel.Country', null=True),
            preserve_default=True,
        ),
    ]

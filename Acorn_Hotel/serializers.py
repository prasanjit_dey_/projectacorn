from rest_framework import serializers
from Acorn_Hotel.models import *

class Hotel_ImagesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Hotel_Images 

        fields = ('image_name','image_heading','image_detail','image_url' )


class HomePage_SliderSerializer(serializers.ModelSerializer):
    slider_image = Hotel_ImagesSerializer(many=True)
    class Meta:
        model = HomePage_Slider 

        fields = ('slider_name','slider_heading','slider_detail','slider_position' ,'slider_image' )

class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country 

        fields = ('country_name' ,)

class CitySerializer(serializers.ModelSerializer):  
    class Meta:
        model = City 

        fields = ('id' ,'city_name' )

class Search_PageSerializer(serializers.ModelSerializer):
    main_image = Hotel_ImagesSerializer()
    nearby_Place_images = Hotel_ImagesSerializer(many=True)
    class Meta:
        model = Search_Page 

        fields = ('searchPage_Heading','searchPage_Detail','main_image','nearby_Place_images' )

class FacilitySerializer(serializers.ModelSerializer):
    facility_image = Hotel_ImagesSerializer()
    class Meta:
        model = Facility 

        fields = ('facility_name','facility_image' ,'slug'  )

class HotelSerializer(serializers.ModelSerializer):
    hotel_images = Hotel_ImagesSerializer(many=True)
    hotel_logo = Hotel_ImagesSerializer()
    hotel_facilities = FacilitySerializer(many=True)
    default_image = Hotel_ImagesSerializer()
    city = CitySerializer()
    class Meta:
        model = Hotel 

        fields = ('hotel_name','hotel_tag_line','hotel_logo','default_image' ,'hotel_description','hotel_street_address','city' ,'pin_code' ,'hotel_longitude','hotel_latiitude' ,'hotel_images','hotel_facilities','hotel_rating','forward_link' )

class RoomSerializer(serializers.ModelSerializer):
    room_images = Hotel_ImagesSerializer(many=True)
    amenities = FacilitySerializer(many=True)
    class Meta:
        model = Room 

        fields = ('room_name','hotel_name','room_price','room_images','amenities' )        

class Price_ListSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Price_List

        fields = ('min_price','max_price')

class SortBy_ListSerializer(serializers.ModelSerializer):
    class Meta:
        model = SortBy_List

        fields = ('sort_by_category_name','is_active')
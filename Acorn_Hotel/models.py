from django.db import models
from django.template.defaultfilters import slugify

# from django.core.exceptions import ValidationError

# Create your models here.
class Hotel_Images(models.Model):
	image_name = models.CharField(max_length = 100,null = True, blank = True )
	image_heading = models.CharField(max_length = 100,null = True, blank = True )
	image_detail = models.TextField(null = True, blank = True )
	image_url = models.ImageField(upload_to='images',blank = True, null = True)
	is_active = models.BooleanField(default = False)

	def __str__(self):
		return self.image_name

class HomePage_Slider(models.Model):
	slider_name = models.CharField(max_length = 100,null = True, blank = True )
	slider_heading = models.CharField(max_length = 100,null = True, blank = True )
	slider_detail = models.TextField(null = True, blank = True )
	slider_position = models.PositiveIntegerField(default = 0)
	slider_image = models.ManyToManyField(Hotel_Images, blank = True, null = True)
	is_active = models.BooleanField(default = False)


	# def save(self, *args, **kwargs):
	# 	no_of_images = len(self.slider_image.all())
	# 	print "no_of_images",no_of_images
	# 	if no_of_images<3 or no_of_images >5 :
	# 		print "inside if "
			
	# 		# raise ValidationError("Item already booked for those dates")
	# 	else:
	# 		super(HomePage_Slider, self).save(*args, **kwargs)

	def __str__(self):
		return self.slider_name

class Country(models.Model):
	country_name = models.CharField(max_length = 50 )
	is_active = models.BooleanField(default = False)
	def __str__(self):
		return self.country_name

class City(models.Model):
	city_name = models.CharField(max_length = 50)
	country_name = models.ForeignKey(Country, blank = True,null = True)
	is_active = models.BooleanField(default = False)

	def __str__(self):
		return self.city_name

class Search_Page(models.Model):
	searchPage_Heading = models.CharField(max_length = 150,null = True, blank = True )
	searchPage_Detail = models.TextField(null = True, blank = True )
	main_image = models.ForeignKey(Hotel_Images,null = True, blank = True,related_name = 'hotel_images' )
	nearby_Place_images = models.ManyToManyField(Hotel_Images,null = True, blank = True ,related_name = 'nearby_places_images')
	is_active = models.BooleanField(default = False)

	def __str__(self):
		return self.searchPage_Heading

class Facility(models.Model):
	facility_name = models.CharField(max_length = 100)
	facility_image = models.ForeignKey(Hotel_Images, blank = True, null = True)
	slug = models.CharField(max_length = 101,null = True, blank = True)
	is_active = models.BooleanField(default = False)

	def save(self, *args, **kwargs):
		self.slug = slugify(self.facility_name)
		super(Facility, self).save(*args, **kwargs)

	def __str__(self):
		return self.facility_name

class Hotel(models.Model):

	hotel_name = models.CharField(max_length = 200)
	hotel_tag_line =  models.CharField(max_length = 200, null = True, blank = True)
	hotel_logo = models.ForeignKey(Hotel_Images, blank = True, null = True, related_name = 'hotel_logo')
	hotel_street_address = models.CharField(max_length = 300, null = True, blank = True)
	city = models.ForeignKey(City,null = True, blank = True)
	pin_code = models.PositiveIntegerField(max_length=6, null = True, blank = True,default = 0)
	hotel_description = models.TextField(null = True, blank = True)
	hotel_longitude = models.CharField(null = True, blank = True,max_length = 100)
	hotel_latiitude = models.CharField(null = True, blank = True,max_length = 100)
	default_image = models.ForeignKey(Hotel_Images, blank = True, null = True, related_name = 'default_image')
	hotel_images = models.ManyToManyField(Hotel_Images,blank = True, null = True,related_name = 'slider_images')
	Rating = (('2','2'),('3','3'),('4','4'),('5','5'))
	hotel_rating = models.CharField(choices = Rating, max_length = 8,default = '2')
	forward_link = models.CharField(max_length =300, blank = True, null = True )
	hotel_facilities = models.ManyToManyField(Facility,blank = True, null = True)
	is_active = models.BooleanField(default = False)

	def __str__(self):
		return self.hotel_name

class Room(models.Model):
	room_name = models.CharField(max_length = 30)
	hotel_name = models.ForeignKey(Hotel,blank = True, null = True,)
	room_price = models.CharField(max_length = 10, default =  '0')
	
	room_images = models.ManyToManyField(Hotel_Images,blank = True, null = True)
	# is_available = models.BooleanField(default = True)
	amenities = models.ManyToManyField(Facility,blank = True, null = True)

	check_in = models.DateField(null = True,blank = True)
	check_out = models.DateField(null = True,blank = True)

	is_active = models.BooleanField(default = False)

	def __str__(self):
		return self.room_name

class Price_List(models.Model):
	min_price = models.PositiveIntegerField(default = 0,max_length = 10)
	max_price = models.PositiveIntegerField(default = 0,max_length = 10)
	is_active = models.BooleanField(default = True)

	def __str__(self):
		return str(self.min_price)+" - "+str(self.max_price)

class SortBy_List(models.Model):
	sort_by_category_name = models.CharField(max_length = 50)
	is_active = models.BooleanField(default = True)

	def __str__(self):
		return self.sort_by_category_name
